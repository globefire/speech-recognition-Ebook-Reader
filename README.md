<h3>Implementing speech synthesis(📖 to 🗣) on eBooks</h3>

Bored of writing notes in a lecture? How about we convert the notes dictated by the lecturer into text?
Use the speechtotext.py script to get the text format of spoken notes, which saves the text in a .txt file.

Too lazy to read a novel? Get an Ebook version of the novel and run the ebook_reader.py script. It will generate an mp3(audio) format of the book. Enjoy book listening :)

An example of each of the script is shown, the text from spoken text.PNG was delivered by me, observe the s2txt.txt it is the o/p (accurate to a great extent if you have a decent accent)
Listen to the read.mp3 and observe the page numbers which are spoken from the pdf file. (eg of ebook reader) 
